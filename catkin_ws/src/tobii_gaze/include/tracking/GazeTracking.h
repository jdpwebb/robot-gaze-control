/**************************************************************************
 *
 * Author: Jeremy Webb
 * Date: July 13, 2015
 * File: GazeTracking.h
 * Description: A class to interface with the tobii gaze sdk
 *
 *************************************************************************/

#pragma once

#include <boost/circular_buffer.hpp>
#include <boost/function.hpp>

#include "tobii_gaze/tobiigaze.h"
#include "tobii_gaze/tobiigaze_error_codes.h"
#include "tobii_gaze/tobiigaze_discovery.h"

#include "tracking/GazeDataHolder.h"

namespace tobii{
	// callback typedef for generated and filtered gaze data
	typedef boost::function<void (const GazeDataHolder::GazeData&)> gazeDataCallback;
	class GazeTracking{
		public:
			static const int URL_SIZE = 256; // size of tracker url string
			// could throw exception of type GazeTrackingException
			GazeTracking(unsigned int gazeSize = GazeDataHolder::DEFAULT_GAZE_DATA_SIZE);
			~GazeTracking();
			// registers a callback for gaze data
			// this overwrites the callback specified in startTracking
			void registerCallback(gazeDataCallback gaze_callback);
			// start eye tracking
			// could throw exception of type GazeTrackingException
			void startTracking(gazeDataCallback gaze_callback);
			// stop eye tracking
			// could throw exception of type GazeTrackingException
			void stopTracking();
			// query if the device is currently tracking
			bool isCurrentlyTracking();
			// draw the current gaze point (after filtering) on the screen
			void drawGazePoint();
		private:
			tobiigaze_eye_tracker* eye_tracker; // eye tracker instance
		        char url[URL_SIZE]; // eye tracker url
		        tobiigaze_error_code error_code; // tobii gaze sdk error_code
			bool isTracking; // boolean value that specifies if the tracker is currently tracking
			GazeDataHolder dataContainer; // store the last gaze points
			gazeDataCallback gazeCallback; // current registered gaze data callback

			// if there is an error, handle it with a custom message
			// will throw an exception of type GazeTrackingException
			void handleErrorMessage(std::string message); 

			// callbacks for direct use with tobii gaze sdk
			// handle an error if there is one
			// could throw an exception of type GazeTrackingException
			friend void handleError(tobiigaze_error_code error_code, void *user_data);
			friend void start_tracking_callback(tobiigaze_error_code error_code, void *user_data);
			friend void on_gaze_data(const tobiigaze_gaze_data* gazedata, const tobiigaze_gaze_data_extensions* extensions, void *user_data);

		public:
			// custom exception for any errors encountered while tracking
			class GazeTrackingException : public std::runtime_error {
				public:
					GazeTrackingException(const std::string& mess) 
						: std::runtime_error(mess) {}
			};
};

	// handles an error if there is one
	void handleError(tobiigaze_error_code error_code, void *user_data);
	// called after tracking has started
	void start_tracking_callback(tobiigaze_error_code error_code, void *user_data);
	// called everytime there is new gaze data
	void on_gaze_data(const tobiigaze_gaze_data* gazedata, const tobiigaze_gaze_data_extensions* extensions, void *user_data);
}
