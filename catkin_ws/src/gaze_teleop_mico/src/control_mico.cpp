/******************************************************************************
 * 
 * Author: Jeremy Webb
 * Date: January, 2016
 * File: control_mico.cpp
 * Description: A ros node which provides main control linking the gaze tracking and mico robot.
 * 
 *****************************************************************************/

#include <ros/ros.h>
#include <signal.h>
#include <ros/xmlrpc_manager.h>
#include <actionlib/client/simple_action_client.h>

#include <dynamic_reconfigure/server.h>
#include <dynamic_reconfigure/BoolParameter.h>
#include <dynamic_reconfigure/IntParameter.h>
#include <dynamic_reconfigure/Reconfigure.h>
#include <dynamic_reconfigure/Config.h>

#include "gaze_teleop_mico/ControlConfig.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/PointStamped.h"
#include "geometry_msgs/Point32.h"
#include <sensor_msgs/PointCloud2.h>
#include "std_msgs/String.h"
#include <tf/tf.h>
#include <tf/transform_listener.h>
#include "boost/thread/mutex.hpp"

#include <string>
#include <math.h>
#include <limits>

#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/common/centroid.h>

#include "jaco_driver/jaco_types.h"
#include "jaco_driver/jaco_pose_action.h"
#include "jaco_driver/jaco_fingers_action.h"
#include "jaco_msgs/HomeArm.h"

#include "tobii_gaze/GazeData.h"
#include "tobii_gaze/MarkerDisplay.h"
#include "std_srvs/Empty.h"
#include "std_srvs/SetBool.h"

class ControlMico{
	public:
		typedef pcl::PointXYZRGB PointT;

		enum MODE{SUSPEND, GAZE_CONTROL};
		enum MOTION{MOVE, CLOSE_HAND, OPEN_HAND};
		enum HAND_STATE{OPEN, CLOSED, PARTIALLY_CLOSED};
	
	// CONSTANTS
		static const float ROBOT_DISTANCE = 2.0;
		static const float TARGET_SPREAD_LIMIT = 0.05;
		const float GRASP_OFFSET = 0.02f;
		const float APPROACH_OFFSET = 0.07f;
		// number to subtract off of max finger position value
		const float FINGER_CLOSE_TOLERANCE = 200.0f;
		// tolerance for determining when the robot is at a specific point (m)
		const float TOOL_POSITION_TOLERANCE = 0.05f;
		// tolerance for determining when the robot tool is in a specific orienation (quaternion
		const float TOOL_ORIENTATION_TOLERANCE = 0.1f;
		// first dimensions is x, y, z
		// second is initial position, lower limit, upper limit
		static const float JACO_LIMITS[][3];
		// finger limits
		static const float JACO_FINGER_LIMITS[3];
		// ready pose
		// should be const, but geometry_msgs doesn't have a constructor and I don't feel like putting together a function to fill it out right now
		geometry_msgs::Pose READY_POSE;
		// cloud topic name
		static const std::string CLOUD_TOPIC;
		// robot finger position topic
		static const std::string FINGER_TOPIC;
		// number of points per side (of a square) to take into account when determining the depth of the target location from the gaze
		static const unsigned int GAZE_DEPTH_FILTER_SIZE = 5;
		
	// Public methods
		ControlMico(std::string touchName = "GTouch");
		// gaze tracker name
		const std::string gazeTrackerName;
		// check if the robot is in a specific pose
		bool isInPose(const geometry_msgs::PoseStamped& robotPose, const geometry_msgs::Pose& checkPose);
		// check the state of the robot hand
		HAND_STATE getHandState();
		static void cleanUp();

	private:
		ros::NodeHandle nh;

		// static parameters
		float grasp_offset; // grasp offset in z direction
		float approach_offset; // approach pose offset in z direction
		// finger position must be within specified tolerance of max position to be considered closed
		float finger_close_tolerance;
		// tolerance for determining when the robot is at a specific point (m)
		float tool_position_tolerance;
		// tolerance for determining when the robot tool is in a specific orienation (quaternion
		float tool_orientation_tolerance;

		// action clients
		actionlib::SimpleActionClient<jaco_msgs::ArmPoseAction> armClient;
		actionlib::SimpleActionClient<jaco_msgs::SetFingersPositionAction> fingersClient;

		dynamic_reconfigure::Server<gaze_teleop_mico::ControlConfig> configServer;
		dynamic_reconfigure::ReconfigureRequest srv_req;
		dynamic_reconfigure::ReconfigureResponse srv_resp;
		dynamic_reconfigure::BoolParameter feedback_enabled;

		ros::Subscriber fingersUpdater;
		ros::Subscriber toolPositionUpdater;
		ros::Subscriber gazeUpdater;
		ros::Subscriber dwellEvent;
		ros::Subscriber cloudSubscriber;
		ros::Subscriber clickSubscriber;

		ros::Publisher instructionsPublisher;
		ros::Publisher ballPointPublisher;

		ros::ServiceClient gazeClientStart;
		ros::ServiceClient gazeClientStop;
		ros::ServiceClient gazeMarkerControl;
		ros::ServiceClient jacoHome;

		tf::TransformListener kinectPointTransformer;

		// current tool position
		geometry_msgs::PoseStamped toolPos;
		// current finger positions
		jaco_msgs::FingerPosition fingerPos;
		// current gaze data
		tobii_gaze::GazeData gazePoint;
		// is the current gazePoint valid
		bool isGazeValid;
		// point cloud
		pcl::PointCloud<PointT> currentCloud;
		// mode of operation
		MODE mode;
		// pause the motion or not
		bool pauseMotion;
		// lock for toolPos
		boost::mutex toolPosLock;
		// lock for fingerPos
		boost::mutex fingerPosLock;
		// lock for the point cloud
		boost::mutex cloudLock;
		// lock for gaze point
		boost::mutex gazeLock;

		// state of the system (1-6)
		unsigned short state;
		
		// update the control configuration
		void updateConfig(gaze_teleop_mico::ControlConfig &config, int32_t level);

		// update tool position
		void updateToolPos(geometry_msgs::PoseStamped::ConstPtr tool);
		// update finger position
		void updateFingers(jaco_msgs::FingerPosition::ConstPtr fingers);
		// update the gaze information
		//void updateGazePoint(tobii_gaze::GazeData::ConstPtr gazePoint);
		void activateDwellEvent(tobii_gaze::GazeData::ConstPtr gaze);
		// process the current activated state
		void updateState(geometry_msgs::Point gazepoint);
		// give instructions
		void sendInstructions();
		void sendInstructions(std::string);
		// get the current state
		unsigned short getCurrentState();
		// get the 3D gaze point
		geometry_msgs::Point getGazeTarget();
		// pick up an object at the indicated location
		bool pickUpObject(geometry_msgs::Point point);
		// put down object at the indicated location
		bool putDownObject(geometry_msgs::Point point);
		// check that the action has completed successfully
		bool checkForActionCompletion();
		// sends joint commands to the arm
		bool sendMotionCommands(geometry_msgs::Pose, MOTION cmd);
		// go to ready position
		bool goToReadyPose();
		// callback for kinect point cloud
		void saveCloud(pcl::PointCloud<PointT>::ConstPtr);
		void saveClick(geometry_msgs::Point32::ConstPtr point);
};

// value which determines when the node is shutdown
bool terminate = false;

// inner dimension is zero, lower limit, upper limit
const float ControlMico::JACO_LIMITS[][3] = {{0.0, 0, -M_PI}, // x 
					 	{-0.25, 0.7252, -1.8000},
					 	{0.4, 0.7681, -1.7000}};
// initial position, 0, max
const float ControlMico::JACO_FINGER_LIMITS[3] = {0, 0, 6400.0};


// cloud topic name
const std::string ControlMico::CLOUD_TOPIC = "/camera/depth_registered/points";

// jaco finger topic name
const std::string ControlMico::FINGER_TOPIC = "/jaco_arm_driver/out/finger_position";

ControlMico::ControlMico(std::string touchName)
	: nh("gaze_teleop_node"), armClient("/jaco_arm_driver/arm_pose/arm_pose"), fingersClient("/jaco_arm_driver/fingers/finger_positions"), gazeTrackerName("tobii"), mode(SUSPEND), pauseMotion(false), kinectPointTransformer(ros::Duration(60.0)), state(1) {
	// initialize parameters
	nh.param<float>("grasp_offset", grasp_offset, GRASP_OFFSET);
	nh.param<float>("approach_offset", approach_offset, APPROACH_OFFSET);
	nh.param<float>("finger_close_tolerance", finger_close_tolerance, FINGER_CLOSE_TOLERANCE);
	nh.param<float>("tool_position_tolerance", tool_position_tolerance, TOOL_POSITION_TOLERANCE);
	nh.param<float>("tool_orientation_tolerance", tool_orientation_tolerance, TOOL_ORIENTATION_TOLERANCE);

	READY_POSE.position.x = 0.21;
	READY_POSE.position.y = -0.26;
	READY_POSE.position.z = 0.478;
	READY_POSE.orientation.x = 0.582227924199;
	READY_POSE.orientation.y = 0.396040591056;
	READY_POSE.orientation.z = 0.371293623641;
	READY_POSE.orientation.w = 0.605230154208;

	// set up subscribers
	toolPositionUpdater = nh.subscribe("/jaco_arm_driver/out/tool_position", 1, &ControlMico::updateToolPos, this);
	fingersUpdater = nh.subscribe(FINGER_TOPIC, 1, &ControlMico::updateFingers, this);
	gazeUpdater = nh.subscribe("/" + gazeTrackerName + "/dwell", 0, &ControlMico::activateDwellEvent, this);
	cloudSubscriber = nh.subscribe(CLOUD_TOPIC, 0, &ControlMico::saveCloud, this);
	clickSubscriber = nh.subscribe("click_point", 0, &ControlMico::saveClick, this);
	instructionsPublisher = nh.advertise<std_msgs::String>("/provide_feedback/feedback_words", 0);
	ballPointPublisher = nh.advertise<geometry_msgs::Point32>("ball_point", 1);

	// set up service handles
	gazeClientStart = nh.serviceClient<std_srvs::Empty>("/" + gazeTrackerName + "/start_tracking");
	gazeClientStop = nh.serviceClient<std_srvs::SetBool>("/" + gazeTrackerName + "/stop_tracking");
	gazeMarkerControl = nh.serviceClient<tobii_gaze::MarkerDisplay>("/" + gazeTrackerName + "/marker_display");
	jacoHome = nh.serviceClient<jaco_msgs::HomeArm>("/jaco_arm_driver/in/home_arm");
	
	// ensure communication is active
	ROS_INFO("Waiting for communication with Mico to become active");
	armClient.waitForServer();
	ROS_INFO("Waiting for kinect");
	ros::topic::waitForMessage<pcl::PointCloud<PointT> >(CLOUD_TOPIC);
	// set up the callback for the config update
	configServer.setCallback(boost::bind(&ControlMico::updateConfig, this, _1, _2));
	ROS_INFO("Ready to run");
}

// cleanup as node is shutting down
void ControlMico::cleanUp(){
	ROS_INFO("Shutting down node");
	std_srvs::SetBool msg;
	ros::service::call("tobii/stop_tracking", msg);
	ros::param::del("/gaze_teleop_node/mode");
}

// check if the robot is in a specific pose
bool ControlMico::isInPose(const geometry_msgs::PoseStamped& robotPose, const geometry_msgs::Pose& checkPose){
	// check that the pose is within the required distance tolerance and orientation
	return sqrt(pow(checkPose.position.x-robotPose.pose.position.x,2) + 
			pow(checkPose.position.y-robotPose.pose.position.y,2) + 
			pow(checkPose.position.z-robotPose.pose.position.z,2)) < tool_position_tolerance &&
		sqrt(pow(checkPose.orientation.x-robotPose.pose.orientation.x,2) + 
			pow(checkPose.orientation.y-robotPose.pose.orientation.y,2) + 
			pow(checkPose.orientation.z-robotPose.pose.orientation.z,2) + 
			pow(checkPose.orientation.w-robotPose.pose.orientation.w,2)) < tool_orientation_tolerance;
}

// check the robot hand state
ControlMico::HAND_STATE ControlMico::getHandState(){
	fingerPosLock.lock();
	if(fingerPos.finger1 > (JACO_FINGER_LIMITS[2] - finger_close_tolerance) ||
		fingerPos.finger2 > (JACO_FINGER_LIMITS[2] - finger_close_tolerance)){ 
		fingerPosLock.unlock();
		return CLOSED;
	}
	if(fingerPos.finger1 < finger_close_tolerance &&
		fingerPos.finger2 < finger_close_tolerance){
		fingerPosLock.unlock();
		return OPEN;
	}
	fingerPosLock.unlock();
	
	// Attempt to close hand all the way
	geometry_msgs::Pose tmpPose;
	sendMotionCommands(tmpPose, CLOSE_HAND);
	// wait for finger positions to be updated
	ros::topic::waitForMessage<jaco_msgs::FingerPosition>(FINGER_TOPIC);
	fingerPosLock.lock();
	// Check one more time if the fingers are closed
	if(fingerPos.finger1 > (JACO_FINGER_LIMITS[2] - finger_close_tolerance) ||
		fingerPos.finger2 > (JACO_FINGER_LIMITS[2] - finger_close_tolerance)){ 
		fingerPosLock.unlock();
		return CLOSED;
	}
	fingerPosLock.unlock();
	return PARTIALLY_CLOSED;
}

void ControlMico::updateConfig(gaze_teleop_mico::ControlConfig &config, int32_t level){
	std_srvs::Empty msg;
	std_srvs::SetBool stopMsg;
	switch(level){
	case 0:
	switch(config.mode){
		case 0:
			mode = SUSPEND;
			pauseMotion = true;
			if(gazeClientStop.exists() && !gazeClientStop.call(stopMsg)){
				ROS_ERROR("Could not stop gaze tracking");
			}
			break;
		case 1:
			// check that the gaze services are ready
			ROS_INFO("Waiting for gaze tracker");
			gazeClientStart.waitForExistence();
			gazeClientStop.waitForExistence();
			mode = GAZE_CONTROL;
			// set this to false so that no old gaze data is used
			isGazeValid = false;
			pauseMotion = false;
			{
			tobii_gaze::MarkerDisplay msg;
			msg.request.on = true;
			msg.request.freeze = false;
			gazeMarkerControl.call(msg);
			}
			if(!gazeClientStart.call(msg)){
				ROS_ERROR("Could not start gaze tracking");
				mode = SUSPEND;
			}
			sendInstructions();
			break;
		default:
			ROS_ERROR("Unknown mode encountered");
			mode = SUSPEND;
			if(!gazeClientStop.call(stopMsg)){
				ROS_ERROR("Could not stop gaze tracking");
			}
	}
	break;
	case 1:
		{
		dynamic_reconfigure::Config conf;
		if(config.text_feedback){
			feedback_enabled.name = "text_feedback_enabled";
			feedback_enabled.value = true;
			conf.bools.push_back(feedback_enabled);
		}
		else{
			feedback_enabled.name = "text_feedback_enabled";
			feedback_enabled.value = false;
			conf.bools.push_back(feedback_enabled);
		}
		srv_req.config = conf;
		ros::service::call("/kinect_viewer/set_parameters", srv_req, srv_resp);
		if(config.text_feedback){
			ros::Duration(0.2).sleep();
			sendInstructions();
		}
		}
		break;
	case 2:
		{
		dynamic_reconfigure::Config conf;
		if(config.speech_feedback){
			feedback_enabled.name = "speech_feedback_enabled";
			feedback_enabled.value = true;
			conf.bools.push_back(feedback_enabled);
		}
		else{
			feedback_enabled.name = "speech_feedback_enabled";
			feedback_enabled.value = false;
			conf.bools.push_back(feedback_enabled);
		}
		srv_req.config = conf;
		ros::service::call("/sound_feedback/set_parameters", srv_req, srv_resp);
		if(config.speech_feedback){
			ros::Duration(0.2).sleep();
			sendInstructions();
		}
		}
		break;
	case 3:
		dynamic_reconfigure::Config conf;
		dynamic_reconfigure::IntParameter intparam;
		intparam.name = "sound_volume";
		intparam.value = config.sound_volume;
		conf.ints.push_back(intparam);
		srv_req.config = conf;
		ros::service::call("/sound_feedback/set_parameters", srv_req, srv_resp);
		break;
	}
}

void ControlMico::activateDwellEvent(tobii_gaze::GazeData::ConstPtr gaze){
	// set gazePoint to the dwell event location
	gazeLock.lock();
	gazePoint = *gaze;
	gazeLock.unlock();
	geometry_msgs::Point currentGazePoint = getGazeTarget();
	if(isGazeValid){
		
		// turn off tracking display and freeze marker on display
		tobii_gaze::MarkerDisplay msg;
		msg.request.on = true;
		msg.request.freeze = true;
		gazeMarkerControl.call(msg);
		ROS_INFO("Fixation at: %0.3f, %0.3f, %0.3f", currentGazePoint.x, currentGazePoint.y, currentGazePoint.z);
		std_srvs::SetBool stopMsg;
		stopMsg.request.data = true;
		gazeClientStop.call(stopMsg);
		updateState(currentGazePoint);
		// turn on tracking display and unfreeze marker on display
		std_srvs::Empty startMsg;
		gazeClientStart.call(startMsg);
		msg.request.on = true;
		msg.request.freeze = false;
		gazeMarkerControl.call(msg);
		sendInstructions();
	}
	else{
		ROS_WARN("Gaze is not valid");
		ROS_INFO("Fixation at: %0.3f, %0.3f, %0.3f", currentGazePoint.x, currentGazePoint.y, currentGazePoint.z);
	}
}

// process new state
void ControlMico::updateState(geometry_msgs::Point gazePoint){
	state = getCurrentState();
	ROS_INFO("Current state is %d", state);
	geometry_msgs::Pose tmpPose;
	bool failure;
	switch(state){
		case 1: // ready, hand is open
			sendInstructions("Attempting to pick up object");
			if(!pickUpObject(gazePoint)){
				ROS_ERROR("Could not pick up object");
			}
			break;
		case 2: // busy, hand is open
			failure = !goToReadyPose();
			sendInstructions("Attempting to pick up object");
			if(failure || !pickUpObject(gazePoint)){
				ROS_ERROR("Could not pick up object");
			}
			break;
		case 3: // ready, hand is closed
			sendInstructions("Attempting to pick up object");
			if(!sendMotionCommands(tmpPose, OPEN_HAND) || !pickUpObject(gazePoint)){
				ROS_ERROR("Could not open hand or pick up object");
			}
			break;
		case 4: // busy, hand is closed
			failure = !sendMotionCommands(tmpPose, OPEN_HAND) || !sendMotionCommands(READY_POSE, MOVE);
			sendInstructions("Attempting to pick up object");
			if(failure || !pickUpObject(gazePoint)){
				ROS_ERROR("Could not open hand or pick up object");
			}
			break;
		case 5: // ready, hand is closed with object
			sendInstructions("Attempting to place object");
			if(!putDownObject(gazePoint)){
				ROS_ERROR("Could not put down object");
			}
			break;
		case 6:  // busy, hand is closed with object
			failure = !goToReadyPose();
			sendInstructions("Attempting to place object");
			if(failure || !putDownObject(gazePoint)){
				ROS_ERROR("Could not put down object");
			}
			break;
		default:
			ROS_ERROR("Unknown state encountered");
			return;
	}
}

void ControlMico::sendInstructions(){
	if(mode == SUSPEND)
		return;
	state = getCurrentState();
	std_msgs::String instructionMsg;
	switch(state){
		case 1: // ready, hand is open
		case 2: // busy, hand is open
		case 3: // ready, hand is closed
		case 4: // busy, hand is closed
			instructionMsg.data = "Stare at target object";
			break;
		case 5: // ready, hand is closed with object
		case 6:  // busy, hand is closed with object
			instructionMsg.data = "Stare at location to place object";
			break;
		default:
			ROS_ERROR("Unknown state encountered");
			return;
	}
	instructionsPublisher.publish(instructionMsg);
}

void ControlMico::sendInstructions(std::string message){
	if(mode == SUSPEND)
		return;
	std_msgs::String instructionMsg;
	instructionMsg.data = message;
	instructionsPublisher.publish(instructionMsg);
}

// get the current state
unsigned short ControlMico::getCurrentState(){
	toolPosLock.lock();
	bool isInReadyState = isInPose(toolPos, READY_POSE);
	toolPosLock.unlock();
	HAND_STATE handState = getHandState();
	if(isInReadyState){
		if(handState == OPEN){
			return 1;
		}
		if(handState == CLOSED){
			return 3;
		}
		// hand closed with object
		return 5;
	}
	else{
		// pose busy
		if(handState == OPEN){
			return 2;
		}
		if(handState == CLOSED){
			return 4;
		}
		return 6;
	}
}

// update tool position
void ControlMico::updateToolPos(geometry_msgs::PoseStamped::ConstPtr tool){
	toolPosLock.lock();
	toolPos = *tool;
	toolPosLock.unlock();
}

// update finger position
void ControlMico::updateFingers(jaco_msgs::FingerPosition::ConstPtr fingers){
	fingerPosLock.lock();
	fingerPos = *fingers;
	fingerPosLock.unlock();
}

// get 3D target position
geometry_msgs::Point ControlMico::getGazeTarget(){
	geometry_msgs::Point result;
	gazeLock.lock();
	if(gazePoint.isTracked){
		cloudLock.lock();
		unsigned int beginRow = gazePoint.y*currentCloud.height - GAZE_DEPTH_FILTER_SIZE/2;
		unsigned int beginCol = gazePoint.x*currentCloud.width - GAZE_DEPTH_FILTER_SIZE/2;
		unsigned int endCol = gazePoint.x*currentCloud.width + GAZE_DEPTH_FILTER_SIZE/2;
		unsigned int endRow = gazePoint.y*currentCloud.height + GAZE_DEPTH_FILTER_SIZE/2;
		gazeLock.unlock();
		if(beginRow < 0){
			beginRow = 0;
			endRow = GAZE_DEPTH_FILTER_SIZE;
		}
		else if(endRow > currentCloud.height){
			endRow = currentCloud.height-1;
			beginRow = endRow - GAZE_DEPTH_FILTER_SIZE;
		}
		if(beginCol < 0){
			beginCol = 0;
			endCol = GAZE_DEPTH_FILTER_SIZE;
		}
		else if(endCol > currentCloud.width){
			endCol = currentCloud.width-1;
			beginCol = endCol - GAZE_DEPTH_FILTER_SIZE;
		}
		double largestZ = -1, smallestZ = std::numeric_limits<double>::infinity();
		// get region around gaze point
		pcl::CentroidPoint<PointT> gazeTarget;
		PointT tempCloudPoint;
		for(int i = beginRow; i <= endRow; ++i){
			for(int j = beginCol; j <= endCol; ++j){
				tempCloudPoint = currentCloud.at(j,i);
				if(!isnan(tempCloudPoint.z))
					gazeTarget.add(tempCloudPoint);
				if(tempCloudPoint.z > largestZ)
					largestZ = tempCloudPoint.z;
				if(tempCloudPoint.z < smallestZ)
					smallestZ = tempCloudPoint.z;
			}
		}

		cloudLock.unlock();
		// average z values in filered cloud
		PointT centroid;
		gazeTarget.get(centroid);
		geometry_msgs::PointStamped kinectPoint, robotPoint;
		kinectPoint.header.frame_id = "camera_depth_optical_frame";
		kinectPoint.point.x = centroid.x;
		kinectPoint.point.y = centroid.y;
		kinectPoint.point.z = centroid.z;
		kinectPointTransformer.transformPoint("jaco_base", kinectPoint, robotPoint);
		result.x = robotPoint.point.x;
		result.y = robotPoint.point.y;
		result.z = robotPoint.point.z;
		if(result.y < -ROBOT_DISTANCE || result.y > 0 || fabs(largestZ - smallestZ) > TARGET_SPREAD_LIMIT || isnan(result.y) || isnan(result.x) || isnan(result.z)){
				isGazeValid = false;
		}
		else{
			isGazeValid = true;
		}
	}
	else{
		gazeLock.unlock();
		isGazeValid = false;
	}
		return result;
}

// picking up an object
bool ControlMico::pickUpObject(geometry_msgs::Point point){
	geometry_msgs::Pose pose;
	pose.position = point;
	pose.orientation.x = 1.0;
	pose.orientation.y = 0.0;
	pose.orientation.z = 0.0;
	pose.orientation.w = 0.0;
	pose.position.z += approach_offset;
	bool success = true;
	success = sendMotionCommands(pose, MOVE);
	pose.position.z = pose.position.z - approach_offset + grasp_offset;
	success = sendMotionCommands(pose, MOVE);
	sendMotionCommands(pose, CLOSE_HAND);
	if(!success){
		ROS_ERROR("Could not close hand");
	}
	goToReadyPose();
	return success;
}

// putting down an object
bool ControlMico::putDownObject(geometry_msgs::Point point){
	geometry_msgs::Pose pose;
	pose.position = point;
	pose.orientation.x = 1.0;
	pose.orientation.y = 0.0;
	pose.orientation.z = 0.0;
	pose.orientation.w = 0.0;
	pose.position.z += approach_offset;
	bool success = true;
	success = sendMotionCommands(pose, MOVE);
	pose.position.z = pose.position.z - approach_offset + grasp_offset + 0.03;
	success = sendMotionCommands(pose, MOVE);
	success = sendMotionCommands(pose, OPEN_HAND);
	if(!success){
		ROS_ERROR("Could not open hand");
	}
	goToReadyPose();
	return success;
}

// Continously sends joint commands to the arm
bool ControlMico::sendMotionCommands(geometry_msgs::Pose pose, MOTION cmd){
	bool success = true;
	if(!pauseMotion){
		switch(cmd){
			case MOVE:
				{
				jaco_msgs::ArmPoseGoal goal;
				goal.pose.pose = pose;
				goal.pose.header.stamp = ros::Time::now();
				goal.pose.header.frame_id = "jaco_api_origin";
				if(goal.pose.pose.position.z < 0){
					ROS_ERROR("Requested z position is too low");
				}
				ROS_INFO("Moving to pose");
				armClient.sendGoal(goal);
				if(!armClient.waitForResult(ros::Duration(60))){
					armClient.cancelAllGoals();
					ROS_ERROR("Motion goal timed out");
				}
				actionlib::SimpleClientGoalState actionState = armClient.getState();
				success = actionState == actionlib::SimpleClientGoalState::SUCCEEDED;
				}
				break;
			case CLOSE_HAND:
				{
				jaco_msgs::SetFingersPositionGoal fingersClose;
				fingersClose.fingers.finger1 = JACO_FINGER_LIMITS[2] - finger_close_tolerance;
				fingersClose.fingers.finger2 = JACO_FINGER_LIMITS[2] - finger_close_tolerance;
				fingersClose.fingers.finger3 = JACO_FINGER_LIMITS[2] - finger_close_tolerance;
				fingersClient.sendGoal(fingersClose);
				if(!fingersClient.waitForResult(ros::Duration(60))){
					fingersClient.cancelAllGoals();
					ROS_ERROR("Motion goal timed out");
				}
				actionlib::SimpleClientGoalState fingersState = fingersClient.getState();
				success = fingersState == actionlib::SimpleClientGoalState::SUCCEEDED;
				}
				break;
			case OPEN_HAND:
				{
				ROS_INFO("Opening fingers");
				jaco_msgs::SetFingersPositionGoal fingersOpen;
				fingersOpen.fingers.finger1 = 0.0;
				fingersOpen.fingers.finger2 = 0.0;
				fingersOpen.fingers.finger3 = 0.0;
				fingersClient.sendGoal(fingersOpen);
				if(!fingersClient.waitForResult(ros::Duration(60))){
					fingersClient.cancelAllGoals();
					ROS_ERROR("Motion goal timed out");
				}
				actionlib::SimpleClientGoalState fingersState = fingersClient.getState();
				success = fingersState == actionlib::SimpleClientGoalState::SUCCEEDED;
				}
				break;
		}
	}
	return true;
	//return success;
}

bool ControlMico::goToReadyPose(){
	sendInstructions("Going to ready pose");
	jaco_msgs::HomeArm tmp;
	if(!jacoHome.call(tmp)){
		ROS_ERROR("Could not home arm");
	}
}

// save the latest point cloud from the kinect
void ControlMico::saveCloud(pcl::PointCloud<PointT>::ConstPtr cloud){
	cloudLock.lock();
	currentCloud = *cloud;
	cloudLock.unlock();
}

void ControlMico::saveClick(geometry_msgs::Point32::ConstPtr point){
	geometry_msgs::Point32 result;
	int screenX = 1920; int screenY = 1080;
	unsigned int beginRow = point->y*currentCloud.height/screenY - GAZE_DEPTH_FILTER_SIZE/2;
	unsigned int beginCol = point->x*currentCloud.width/screenX - GAZE_DEPTH_FILTER_SIZE/2;
	unsigned int endCol = point->x*currentCloud.width/screenX + GAZE_DEPTH_FILTER_SIZE/2;
	unsigned int endRow = point->y*currentCloud.height/screenY + GAZE_DEPTH_FILTER_SIZE/2;
	cloudLock.lock();
	if(beginRow < 0){
		beginRow = 0;
		endRow = GAZE_DEPTH_FILTER_SIZE;
	}
	else if(endRow > currentCloud.height){
		endRow = currentCloud.height-1;
		beginRow = endRow - GAZE_DEPTH_FILTER_SIZE;
	}
	if(beginCol < 0){
		beginCol = 0;
		endCol = GAZE_DEPTH_FILTER_SIZE;
	}
	else if(endCol > currentCloud.width){
		endCol = currentCloud.width-1;
		beginCol = endCol - GAZE_DEPTH_FILTER_SIZE;
	}
		// get region around gaze point
		pcl::CentroidPoint<PointT> gazeTarget;
		PointT tempCloudPoint;
		for(int i = beginRow; i <= endRow; ++i){
			for(int j = beginCol; j <= endCol; ++j){
				tempCloudPoint = currentCloud.at(j,i);
				if(!isnan(tempCloudPoint.z))
					gazeTarget.add(tempCloudPoint);
			}
		}
		cloudLock.unlock();

		// average z values in filered cloud
		PointT centroid;
		gazeTarget.get(centroid);
		geometry_msgs::PointStamped kinectPoint, robotPoint;
		kinectPoint.header.frame_id = "camera_depth_optical_frame";
		kinectPoint.point.x = centroid.x;
		kinectPoint.point.y = centroid.y;
		kinectPoint.point.z = centroid.z;
		kinectPointTransformer.transformPoint("jaco_base", kinectPoint, robotPoint);
		result.x = robotPoint.point.x;
		result.y = robotPoint.point.y;
		result.z = robotPoint.point.z;
		
		ballPointPublisher.publish(result);
}

// custom shutdown code for ROS
void shutdown(int sig){
	terminate = true;
	ControlMico::cleanUp();
	ros::shutdown();
}

void shutdownCallback(XmlRpc::XmlRpcValue& params, XmlRpc::XmlRpcValue& result){
	int num_params = 0;
	if(params.getType() == XmlRpc::XmlRpcValue::TypeArray)
		num_params = params.size();
	if(num_params > 1){
		std::string reason = params[1];
		ROS_WARN("Shutdown request received. Reason: [%s]", reason.c_str());
		terminate = true;
		ControlMico::cleanUp();
		ros::shutdown();
	}
	result = ros::xmlrpc::responseInt(1, "", 0);
}

int main(int argc, char **argv){
	ROS_INFO("Initializing gaze_teleop_node");
	ros::init(argc, argv, "gaze_teleop_node");
	ControlMico control;
	// override SIGINT handler
	signal(SIGINT, shutdown);
	// override XMLRPC shutdown
	ros::XMLRPCManager::instance()->unbind("shutdown");
	ros::XMLRPCManager::instance()->bind("shutdown", shutdownCallback);
	ros::AsyncSpinner spinner(3);
	spinner.start();
	ros::waitForShutdown();
	return 0;
}
