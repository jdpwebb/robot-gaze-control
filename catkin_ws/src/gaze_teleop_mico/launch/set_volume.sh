#!/bin/bash
#
# Script to set the volume to a parameter
#
VOLUME=`amixer -D pulse sget Master | grep -m 1 -o -E '[0-9]{1,}%'`
VOLUME="${VOLUME::-1}"
rosparam set /gaze_teleop_node/sound_volume $VOLUME
